require('dotenv').load();

var job = require('./jobInterpreter.js');
var Botkit = require('botkit');
var Sequelize = require('sequelize');

var sequelize = new Sequelize(process.env.MYSQL_DB, process.env.MYSQL_USER, process.env.MYSQL_PASS, {
    host: process.env.MYSQL_HOST,
    dialect: 'mysql',
    pool: {
        max: 500,
        min: 10,
        idle: 5000,
        acquire: 5000,
        evict: 10000,
        handleDisconnects: true
    },
    retry:{max: 3},
    logging: false,
    dialectOptions: {
                requestTimeout: 0,
    }
});

var people = sequelize.define('reamp_people', {
    username: { type: Sequelize.STRING, allowNull: true },
    userid: Sequelize.STRING,
    rmcId: {type: Sequelize.STRING, allowNull: true},
    real_name: { type: Sequelize.STRING, allowNull: true },
    nick_name: { type: Sequelize.STRING, allowNull: true },
    email: { type: Sequelize.STRING, allowNull: true },
    area: { type: Sequelize.STRING, allowNull: true }, // opec, midia e audiencia, tecnologia, bi, administrativo
    cargo: { type: Sequelize.STRING, allowNull: true },
    ramal: { type: Sequelize.STRING, allowNull: true },
    status_cadastro: { type: Sequelize.STRING, allowNull: true }, // esperando cadastro, cadastrada
    params: { type: Sequelize.TEXT, allowNull: true }
});
// Botkit
var middleware = require('botkit-middleware-watson')({
    username: process.env.CONVERSATION_USERNAME,
    password: process.env.CONVERSATION_PASSWORD,
    workspace_id: process.env.WORKSPACE_ID,
    version_date: '2016-09-20'
});

// Configure your bot.
var slackController = Botkit.slackbot();
var slackBot = slackController.spawn({
    token: process.env.SLACK_TOKEN
});

slackController.hears(['.*'], ['direct_message', 'direct_mention', 'mention'], function(bot, message) {
    // Within context where you have a message object
    var currentUser; 
    slackBot.api.users.info({user:message.user},function(err,response) {
        if(err) {
            bot.say("ERROR :(");
        }
        else {
            // console.log(response["user"]);
            var slackName = response["user"]["name"];
            var firstName = response["user"]["profile"]["first_name"];
            var realName = response["user"]["profile"]["real_name"];
            var email = response["user"]["profile"]["email"];
            var teamid = response["user"]["profile"]["team"];

            if(!firstName) {
                firstName = realName;
            }
            
            currentUser = capitalize.words(firstName);

            console.log("REAL NAME " + realName);

            people.findAll({ where: {userid: message.user} }).then(function (resultSet) {                
                var nickName = firstName;
                var person = null;
                // if not found in database with this real name, register
                if(!resultSet || resultSet.length == 0) {
                    var person = {
                        username: slackName,
                        userid: message.user,
                        nick_name: firstName,
                        real_name: realName,
                        email: email
                    };
                    people.create(person);
                } else {
                    var person = resultSet[0];                    
                    nickName = resultSet[0].nick_name;
                }

                if(nickName) capitalize.words(nickName);

                middleware.interpret(bot, message, function (err) {
                    if (!err) {

                        // verificar no mapa de conversation ids, qto tempo foi da ultima msg dessa conversa

                        var timeoutRegistry = {
                            bot: bot,
                            message: message,
                            person: person,
                            lastMessage: new Date().getTime()
                        };

                        var timeoutKey = message.watsonData.context.conversation_id;

                        timeoutMap[timeoutKey] = timeoutRegistry;
                        

                        job.interpretRequest(bot, message, person);
                    }
                });
            });
        }
    });
});